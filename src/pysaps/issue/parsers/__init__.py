from abc import ABC
from typing import List

from pysaps.issue import Issue


class IssueParser(ABC):
    pattern: str

    @classmethod
    def parse(cls, content: str) -> List[Issue]:
        raise NotImplementedError()


__all__ = [
    'clang_tidy',
    'gcc4',
    'clang',
    'msvc',
    'IssueParser'
]
