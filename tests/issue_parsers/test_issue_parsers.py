from pathlib import Path

import pytest
import yaml

from pysaps.issue.parsers import *  # noqa: F403

this_dir = Path(__file__).parent


@pytest.mark.parametrize('klass', IssueParser.__subclasses__())  # noqa: F405
def test_issue_parser_parses_references_are_present(klass):
    content = open(this_dir / f'{klass.__name__}.txt').read()
    expected = yaml.safe_load(open(this_dir / f'{klass.__name__}_expected.yml'))
    issues = klass().parse(content)
    assert len(issues) == len(expected['issues'])
    for c, issue_expected in enumerate(expected['issues']):
        issue_obtained = issues[c]
        for k, expected in issue_expected.items():
            actual = issue_obtained.__dict__[k]
            assert str(actual) == str(expected), f'When comparing key "{k}" on issue #{c}'
